#include <iostream>
#include "classes/Menu.h"
#include "classes/Email.h"
#include "classes/Utils.h"
#include "classes/EmailManager.h"

using namespace std;

void showMainMenu();

void showViewMenu();

void showDeleteMenu();

void showSearchMenu();

Email draft();

void viewAll();

void viewOne(string email);

void deleteOne();

EmailManager manager;

int main() {
    std::cout << "Hello, World!" << std::endl;
    showMainMenu();
    return 0;
}

void deleteOne() {
    viewAll();
    cout << "Index of email to delete" << endl;
    int index;
    cin >> index;
    manager.deleteEmail(manager.getByIndex(index - 1));
}

void viewAll() {
    cout << "***********All Emails****************" << endl;
    list <Email> temp = manager.getAll();
    int count = 1;
    for (list<Email>::iterator i = temp.begin(); i != temp.end(); i++) {
        cout << "Email #" << count << endl;
        i->printPretty();
        count++;
        cout << endl;
    }
}

void viewOne(string email) {
    list <Email> temp = manager.get(email);
    for (list<Email>::iterator i = temp.begin(); i != temp.end(); i++) {
        i->printPretty();
        cout << endl;
    }
}

Email draft() {

    string sender, b, s;
    list <string> rTemp;
    list <Attachment> aTemp;
    cout << "Enter Sender" << endl;
    cin.ignore();
    getline(cin, sender);
    int count = 1;
    while (true) {
        string r;
        cout << "Enter Recipient #" << count << " or /q to stop" << endl;
        cin >> r;
        if (r != "/q") {
            rTemp.push_back(r);
            count++;
        } else {
            break;
        }

    }
    cout << "Enter Subject" << endl;
    cin.ignore();
    getline(cin, s);
    cout << "Enter body" << endl;
    getline(cin, b);
    count = 1;
    while (true) {
        string path;
        cout << "Enter path to Attachment #" << count << " or /q to quit" << endl;
        getline(cin, path);
        if (path != "/q") {
            char arr[Attachment::MAX_SIZE];
            string name = getFileName(path);
            string ext = getFileExtension(path);
            getFileContents(path, arr);
            Attachment temp(name, ext, &arr);
            aTemp.push_back(temp);
            count++;
        } else {
            break;
        }
    }
    time_t rawtime;
    time(&rawtime);
    Email eTemp(sender, rTemp, rawtime, s, b, aTemp);
    eTemp.printPretty();
    return eTemp;
}


void showMainMenu() {
    list <string> mainList;
    mainList.push_back("1. New Email");
    mainList.push_back("2. View Email*");
    mainList.push_back("3. Delete Email*");
    mainList.push_back("4. Search Email*");
    mainList.push_back("5. Reset Email");
    mainList.push_back("6. Exit");
    Menu MainMenu("Main Menu", 1, mainList);
    int choice = 0;

    do {

        choice = MainMenu.showMenuGetChoice("Enter your choice: (0 to exit) ");

        if (choice == 1) {
//            cout << "Choice #1 " << endl;
            Email email = draft();
            cout << "Do you want to send this email ? y/n" << endl;
            char a;
            cin >> a;
            if (a == 'y') {
                manager.add(email);
            } else {
                cout << "Email Discarded " << endl;
            }
        } else if (choice == 2) {
            showViewMenu();
        } else if (choice == 3) {
            showDeleteMenu();
        } else if (choice == 4) {
            showSearchMenu();
        } else if (choice == 5) {
            manager = EmailManager();
            cout << "EmailManager reset!" << endl;
        } else if (choice == 6) {
            cout << "Saving emails" << endl;
            manager.save("savefile.txt");
            cout << "Emails saved, bye." << endl;
            exit(0);
        }


    } while (choice != 0);

    cout << "Goodbye..." << endl;

}

void showDeleteMenu() {
    list <string> deleteList;
    deleteList.push_back("1. Delete Email");
    deleteList.push_back("2. Delete All");
    deleteList.push_back("3. Back to Main Menu");
    Menu DeleteMenu("Delete Menu", 1, deleteList);
    int choice = 0;

    do {

        choice = DeleteMenu.showMenuGetChoice("Enter your choice: (0 to exit) ");

        if (choice == 1) {
            deleteOne();
        } else if (choice == 2) {
            manager.deleteAll();
        } else if (choice = 3) {
            showMainMenu();
        }


    } while (choice != 0);

    cout << "Goodbye..." << endl;

}

void showViewMenu() {
    list <string> viewList;
    viewList.push_back("1. View One Email");
    viewList.push_back("2. View All");
    viewList.push_back("3. Back to Main Menu");
    Menu ViewMenu("New Menu", 1, viewList);
    int choice = 0;

    do {

        choice = ViewMenu.showMenuGetChoice("Enter your choice: (0 to exit) ");

        if (choice == 1) {
            cout << "Enter Email address to view" << endl;
            string add;
            cin >> add;
            viewOne(add);
        } else if (choice == 2) {
            viewAll();
        } else if (choice == 3) {
            showMainMenu();
        }


    } while (choice != 0);

    cout << "Goodbye..." << endl;

}

void showSearchMenu() {
    list <string> searchList;
    searchList.push_back("1. By Attachments");
    searchList.push_back("2. subject");
    searchList.push_back("3. Text in body");
    searchList.push_back("4. Back to Main Menu");
    Menu SearchMenu("Search Menu", 1, searchList);
    int choice = 0;

    do {

        choice = SearchMenu.showMenuGetChoice("Enter your choice: (0 to exit) ");

        if (choice == 1) {
            list <Email> temp = manager.searchByAtt();
            for (list<Email>::iterator i = temp.begin(); i != temp.end(); i++) {
                i->printPretty();
                cout << endl;
            }
        } else if (choice == 2) {
            cout << "Enter subject to search for " << endl;
            string sub;
            cin.ignore();
            getline(cin, sub);
            list <Email> temp = manager.search(sub);
            for (list<Email>::iterator i = temp.begin(); i != temp.end(); i++) {
                i->printPretty();
                cout << endl;
            }
        } else if (choice == 3) {
            cout << "Enter text to search for in body, (regex patterns also valid)" << endl;
            string sub;
            cin.ignore();
            getline(cin, sub);
            list <Email> temp = manager.searchText(sub);
            for (list<Email>::iterator i = temp.begin(); i != temp.end(); i++) {
                i->printPretty();
                cout << endl;
            }
        } else if (choice == 4) {
            showMainMenu();
        } else if (choice == 0) {
            break;
        }


    } while (choice != 0);

    cout << "Goodbye..." << endl;

}
