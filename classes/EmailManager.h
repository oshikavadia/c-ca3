//
// Created by oshi on 12/6/16.
//

#ifndef C_CA3_EMAILMANAGER_H
#define C_CA3_EMAILMANAGER_H

#include "Email.h"
#include "Attachment.h"
#include <cstring>
#include <algorithm>
#include <list>
#include <fstream>
#include <regex>

using namespace std;

class EmailManager {
public:
    Email getByIndex(int i);

    void add(const Email &e);

    list <Email> get(const string &sender);

    virtual ~EmailManager();

    list <Email> getAll();

    void deleteEmail(Email);

    void deleteAll();

    EmailManager();

    list <Email> search(const string &subject);

    list <Email> searchText(const string text);

    list<Email> searchByAtt();


    void load(const string &path);

    void save(const string &path);

private:
    list <Email> emailList;
};


#endif //C_CA3_EMAILMANAGER_H
