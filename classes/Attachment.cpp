//
// Created by oshi on 12/5/16.
//

#include "Attachment.h"

//int Attachment::MAX_SIZE = 1000000;

Attachment::Attachment(const string &FileName, const string &FileSuffix, char (*FileData)[MAX_SIZE]) : FileName(
        FileName),
                                                                                                      FileSuffix(
                                                                                                              FileSuffix),
                                                                                                      FileData(
                                                                                                              *FileData) {}

Attachment::~Attachment() {

}

const string &Attachment::getFileName() const {
    return FileName;
}

void Attachment::setFileName(const string &FileName) {
    Attachment::FileName = FileName;
}

const string &Attachment::getFileSuffix() const {
    return FileSuffix;
}

void Attachment::setFileSuffix(const string &FileSuffix) {
    Attachment::FileSuffix = FileSuffix;
}

const char *Attachment::getFileData() const {
    return FileData;
}

bool Attachment::operator==(const Attachment &rhs) const {
    return FileName == rhs.FileName &&
           FileSuffix == rhs.FileSuffix &&
           FileData == rhs.FileData;
}

bool Attachment::operator!=(const Attachment &rhs) const {
    return !(rhs == *this);
}

ostream &operator<<(ostream &os, const Attachment &attachment) {
    string fileDataString = attachment.FileData;
    replace(fileDataString.begin(), fileDataString.end(), ' ', '_');
    os<< attachment.FileName << "." << attachment.FileSuffix << " "
       << fileDataString;
    return os;
}

string Attachment::toString() const {
    string fileDataString = FileData;
    replace(fileDataString.begin(), fileDataString.end(), ' ', '_');
    replace(fileDataString.begin(), fileDataString.end(), '\n', '\t');
    string returnstring = FileName + " " + FileSuffix + " "
      + fileDataString;
    return returnstring;
}

Attachment::Attachment() {
    (&FileData)[MAX_SIZE] = {};
}
