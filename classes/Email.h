//
// Created by oshi on 12/5/16.
//

#ifndef C_CA3_EMAIL_H
#define C_CA3_EMAIL_H

#include "User.h"
#include "Attachment.h"
#include <ctime>
#include <iostream>
#include <list>

using namespace std;

class Email {
private :
    string Sender;
    list <string> Recipients;
    time_t DateTime;
    string Subject;
    string Body;
    list <Attachment> attachments_list;

public:
    Email();

    Email(const string &Sender, const list <string> &Recipients, time_t DateTime, const string &Subject,
          const string &Body, list <Attachment> attachments_vect);

    const string &getSender() const;

    const list <string> &getRecipients() const;

    time_t getDateTime() const;

    const string &getSubject() const;

    const string &getBody() const;

    list <Attachment> getAttachments_vect() const;

    void setSender(const string &Sender);

    void setRecipients(const list <string> &Recipients);

    void setDateTime(time_t DateTime);

    void setSubject(const string &Subject);

    void setBody(const string &Body);

    void setAttachments_vect(list <Attachment> attachments_vect);

    bool operator==(const Email &rhs) const;

    bool operator!=(const Email &rhs) const;

    friend ostream &operator<<(ostream &os, const Email &email);

    void printPretty();
};


#endif //C_CA3_EMAIL_H
