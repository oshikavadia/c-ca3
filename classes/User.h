//
// Created by oshi on 11/29/16.
//

#ifndef C_CA3_USER_H
#define C_CA3_USER_H

#include <string>
#include <iostream>
#include <regex>

using namespace std;

class User {
private :
    string email;
    string password;
    string userName;
public:
    const string &getEmail() const;

    void setEmail(const string &email);

    const string &getpassword() const;

    void setpassword(const string &password);

    const string &getUserName() const;

    void setUserName(const string &userName);

    friend ostream &operator<<(ostream &os, const User &user);

    User();

    virtual ~User();

    bool operator==(const User &rhs) const;

    bool operator!=(const User &rhs) const;


};


#endif //C_CA3_USER_H
