//
// Created by oshi on 12/5/16.
//

#include <algorithm>
#include <string>
#include <fstream>
#include <iostream>
#include <vector>
#include "Attachment.h"

using namespace std;

void getFileContents(const string &path,char arr[Attachment::MAX_SIZE]);

streampos getFileSize(const string &path);

string getFileName(const string &path);

string getFileExtension(const string &path);

vector<std::string> split(std::string data, std::string delimiter);

template<typename Iter>
Iter nth_occurence(Iter first, Iter last,
                   Iter first_, Iter last_,
                   unsigned nth);